# pdf4page
Pdf4page is a script that was created to allow more effective printing of PDFs. I am fully aware that the term 'effective' is somewhat subjective: let's just say that I like my printouts in A5 format and double-sided. The script re-arranges the supplied PDF after which it should be printed two pages per sheet and double-sided. After printing the stack can easily be cut in half and placed on top of each other, without having to manually re-arrange anything. 

**Note** that since in the final PDF the pages are grouped in fours, that there might not be enough pages to complete final four. For this the script creates empty PDF pages in A4 format. If another format is required, I refer to the StackExchange link under Acknowledgements below.

## Requirements
- This script requires that you install pdftk beforehand, which it uses to split the PDF into separate pages and put them together again after re-arringing;
- This bash script was written on MacOS, which means that it can be used with a bash version below 4.

## Installing the script
To install:
- Download the file (or copy-paste the text into a new file);
- Make it executable with `sudo chmod 755 pdf4page` (or whatever your preferred access rights are).

## Using the script
- `pdf4page my_book.pdf`
- When printing, be sure to select: '2 per page', 'double-sided' and 'flip on short edge'.
If you want to make sure you have your printing setting correctly, it is recommended doing a testprint with a short PDF first.

## Acknowledgements
This bash script was inspired mainly by the following pages:
- https://medium.com/@pourya7/how-to-re-order-pages-in-a-pdf-file-in-command-line-584b5bd8df39
- https://unix.stackexchange.com/questions/277892/how-do-i-create-a-blank-pdf-from-the-command-line#answer-277967